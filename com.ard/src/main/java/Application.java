import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

/**
 * 类 名: Application
 * 描 述:
 * 作 者: 黄加耀
 * 创 建: 2019/11/27 : 19:42
 * 邮 箱: huangjy19940202@gmail.com
 *
 * @author: jiaYao
 */
@Configuration
//@ComponentScan(basePackages = {"com.ard"})
public class Application {

	public static void main(String[] args) {
		/**
		 * 基于包路径扫描
		 */
		ApplicationContext context = new AnnotationConfigApplicationContext("com.ard");
		context.getBean("userComponent");
		/**
		 * 基于XML配置文件
		 */
//		ApplicationContext context1 = new ClassPathXmlApplicationContext("myBeans.xml");
//		/**
//		 * 基于配置类
//		 */
//		ApplicationContext context2 = new AnnotationConfigApplicationContext(Application.class);
		String[] names = context.getBeanDefinitionNames();
		for (int i = 0; i < names.length; i++) {
			System.out.println(names[i]);
		}
////		/**
//		 * ......等
//		 */


//		UserService userService = context.getBean(UserService.class);
//		System.out.println(userService);
	}

}
